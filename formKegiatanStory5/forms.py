from django import forms
from django.db import models
from .models import JadwalPribadi
from django.forms import ModelForm, Textarea

class formKegiatan(forms.ModelForm):

    class Meta:
        model = JadwalPribadi
        fields = ('nama', 'tempat', 'kategori', 'tanggal', 'waktu')
        widgets = {
            'nama' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Masukan Nama Kegiatan'}),
            'tempat' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Masukan Tempat'}),
            'kategori' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Masukan Kategori Kegiatan'}),
            'tanggal' : forms.DateInput(attrs={'class': 'form-control', 'placeholder': 'Masukan Tanggal Kegiatan', 'type': 'date'}),
            'waktu' : forms.TimeInput(attrs={'class': 'form-control', 'placeholder': 'Masukan Tempat', 'type': 'time'}),
        }