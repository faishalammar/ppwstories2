from .models import JadwalPribadi
from django.shortcuts import render, reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render,redirect
from django.http import HttpResponse
from datetime import datetime
from .forms import formKegiatan


# Create your views here.
def formjadwal(request):
    form = formKegiatan(request.POST)
    if request.method == "POST":
        form.save()
        return HttpResponseRedirect(reverse('hasilForm'))
    else:
        form = formKegiatan()
    return render(request, 'formKegiatanStory5/post_edit.html', {'form': form})

def message_table(request):
    data = JadwalPribadi.objects.all()
    response = {'data': data}
    return render(request, 'formKegiatanStory5/post_detail.html', response)

def message_delete(request):
    data = JadwalPribadi.objects.all().delete()
    response = {'data': data}
    return redirect('hasilForm')
