from django.urls import path
from . import views

urlpatterns = [
    path('formKegiatan', views.formjadwal, name='formjadwal'),
    path('hasilForm',views.message_table,name='hasilForm'),
    path('hasilFormDelete', views.message_delete, name='hasilFormDelete'),
]