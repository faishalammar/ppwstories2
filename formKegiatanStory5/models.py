from django.db import models
from django.utils import timezone

# Create your models here.


class JadwalPribadi(models.Model):
    nama = models.CharField(max_length=120)
    tempat = models.CharField(max_length=120)
    kategori = models.CharField(max_length=120)
    tanggal = models.DateField()
    waktu = models.TimeField()