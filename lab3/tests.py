from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from formKegiatanStory5.forms import formKegiatan
from .views import *
from formKegiatanStory5.views import *
from formKegiatanStory5.models import JadwalPribadi
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

import unittest


# Create your tests here.

class Story8UnitTest(TestCase):

    def test_home_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)


    def test_profile_page_is_exist(self):
        response = Client().get('/profil')
        self.assertEqual(response.status_code,200)

    def test_using_buku_func(self):
        found = resolve('/profil')
        self.assertEqual(found.func, buku)

    def test_hasilForm_page_is_exist(self):
        response = Client().get('/hasilForm')
        self.assertEqual(response.status_code,200)

    def test_using_message_table_function(self):
        found = resolve('/hasilForm')
        self.assertEqual(found.func, message_table)

    def test_post_form_success_and_render_the_result(self):
        nama_str = 'abc'
        tempat_str = 'fasilkom'
        kategori_str = 'belajar'
        tanggal_str = '12/12/1999'
        waktu_str = '12:34'

        response_post = Client().post('/formKegiatan', {
            'nama' : nama_str,
            'tempat' : tempat_str,
            'kategori' : kategori_str,
            'tanggal' : tanggal_str,
            'waktu' : waktu_str
        })
        self.assertEqual(response_post.status_code, 302)
        response = Client().get('/hasilForm')
        html_response = response.content.decode('utf8')
        self.assertIn(nama_str, html_response)
        self.assertIn('Dec. 12, 1999', html_response)
        self.assertIn(tempat_str, html_response)
        self.assertIn('12:34 p.m.', html_response)
        self.assertIn(kategori_str, html_response)

# class Story8FunctionalTest(TestCase):
#     def setUp(self):
#         chrome_options = Options()
#         # chrome_options.add_argument('--dns-prefetch-disable')
#         # chrome_options.add_argument('--no-sandbox')
#         # chrome_options.add_argument('--headless')
#         # chrome_options.add_argument('disable-gpu')
#         service_log_path= './chromedriver.log'
#         service_args= ['--verbose']
#         self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         self.selenium.implicitly_wait(25)
#
#         super(Lab6FunctionalTest, self).setUp()
#
#     def tearDown(self):
#         self.selenium.quit()
#         super(Story8FunctionalTest, self).tearDown()
#
#     def test_input_todo(self):
#         selenium = self.selenium
#         # Opening the link we want to test
#         html_response = selenium.get('http://127.0.0.1:8000')
#         time.sleep(3)
#         # find the form element
#         status = selenium.find_element_by_name('status')
#         submit = selenium.find_element_by_name('submit')
#         # Fill the form with data
#         input = 'coba coba'
#         status.send_keys(input)
#         time.sleep(3)
#         # submitting the form
#         submit.send_keys(Keys.RETURN)
#         self.assertIn(input,selenium.page_source )
#
#     def test_Story_5_layout_1(self):
#         selenium = self.selenium
#         #  ------------- http://ppwstories.herokuapp.com/ ---------------------
#         home_url = 'http://ppwstories.herokuapp.com/'
#         selenium.get(home_url)
#
#         # Nama
#         element_nama  = selenium.find_element_by_class_name('nama')
#         nama = element_nama.text
#         self.assertEqual(nama,'Faishal Ammar')
#
#
#     def test_story_5_cek_ada_tombol_profile_dan_mengarah_ke_halaman_yang_di_ekspektasikan(self):
#         selenium = self.selenium
#         home_url = 'http://ppwstories.herokuapp.com/halamanBuku'
#         selenium.get(home_url)
#
#         tombol_kembali = selenium.find_element_by_class_name('btn')
#         tombol_kembali.click()
#         time.sleep(3)
#         #  ------------- http://ppwstories.herokuapp.com/ ---------------------
#         url_after_click_back_button = selenium.current_url
#
#         # Cek apakah url ketika di pencet tombol kembali, mengarah ke laman home awal
#         self.assertEqual(url_after_click_back_button,'http://ppwstories.herokuapp.com/')
#
#
#     def test_cek_css_background_color(self):
#         selenium = self.selenium
#         #  ------------- http://ppwstories.herokuapp.com/ ---------------------
#         home_url = 'http://ppwstories.herokuapp.com/'
#         selenium.get(home_url)
#
#         # Check CSS - Background Image
#         element_mainframe = selenium.find_element_by_class_name('mainFrame')
#         background_image = element_mainframe.value_of_css_property('background-image')
#         self.assertEqual(background_image, 'url("https://i.postimg.cc/FHBGSDTG/Webp.net-resizeimage.jpg")')
#
#         element_blue_layer = selenium.find_element_by_class_name('layer')
#         layer_color = element_blue_layer.value_of_css_property('background-color')
#         self.assertEqual(layer_color, 'rgba(56, 117, 215, 1)')
#
#     def test_Story_5_css_cek_warna_font(self):
#         selenium = self.selenium
#         #  ------------- http://ppwstories.herokuapp.com/ ---------------------
#         home_url = 'http://ppwstories.herokuapp.com/'
#         selenium.get(home_url)
#         # Nama dan deskripsi
#         nama  = selenium.find_element_by_class_name('nama')
#         warna_font_nama = nama.value_of_css_property('color')
#         self.assertEqual(warna_font_nama,'rgba(255, 255, 255, 1)')
#         deskripsi = selenium.find_element_by_class_name('capt')
#         warna_font_deskripsi = deskripsi.value_of_css_property('color')
#         self.assertEqual(warna_font_deskripsi,'rgba(255, 255, 255, 1)')
